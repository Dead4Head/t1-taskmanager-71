package ru.t1.amsmirnov.taskmanager.integration.rest;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.amsmirnov.taskmanager.dto.Result;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/tasks/";

    @Nullable
    private TaskWebDto task1;

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(url, Result.class, Result.class);
        System.out.println(response);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header));
    }

    private static ResponseEntity<TaskWebDto> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskWebDto.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = PROJECT_URL + "create?name=TEST_1";
        task1 = sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header)).getBody();
    }

    @After
    public void clean() {
        @NotNull final String url = PROJECT_URL + "deleteAll/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @Test
    public void saveTest() {
        task1.setName(task1.getName()+"_UPDATED");
        @NotNull final String expected = task1.getName();
        @NotNull final String url = PROJECT_URL + "save/";
        @NotNull final ResponseEntity<TaskWebDto> response = sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final TaskWebDto task = response.getBody();
        Assert.assertNotNull(task);
        @NotNull final String actual = task.getName();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void findByIdTest() {
        @NotNull final String id = task1.getId();
        @NotNull final String url = PROJECT_URL + "findById/" + id;
        @NotNull final ResponseEntity<TaskWebDto> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        @Nullable final TaskWebDto task = response.getBody();
        Assert.assertNotNull(task);
        final String actual = task.getId();
        Assert.assertEquals(id, actual);
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = task1.getId();
        @NotNull final String url = PROJECT_URL + "delete/" + id;
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        @NotNull final String urlFind = PROJECT_URL + "findById/" + id;
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        Assert.assertFalse(restTemplate.exchange(urlFind, HttpMethod.GET, new HttpEntity<>(header), Result.class).getBody().getSuccess());
    }

}
