package ru.t1.amsmirnov.taskmanager.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.config.WebApplicationConfiguration;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.user.AccessDeniedException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.repository.dto.ProjectDtoRepository;
import ru.t1.amsmirnov.taskmanager.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class})
@Category(DBCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private final ProjectWebDto project1 = new ProjectWebDto("Test Project 1");

    @NotNull
    private final ProjectWebDto project2 = new ProjectWebDto("Test Project 2");

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() throws AccessDeniedException {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @After
    public void clean() throws AbstractException {
        projectRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllByUserIdTest() throws AbstractException {
        Assert.assertNotNull(projectRepository.findAllByUserId(UserUtil.getUserId()));
        Assert.assertEquals(2, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByUserIdAndIdTest() throws AbstractException {
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
        Assert.assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project2.getId()));
    }

    @Test
    public void deleteAllByUserIdTest() throws AbstractException {
        projectRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdAndIdTest() throws AbstractException {
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

}
