package ru.t1.amsmirnov.taskmanager.unit.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.amsmirnov.taskmanager.config.DatabaseConfiguration;
import ru.t1.amsmirnov.taskmanager.config.WebApplicationConfiguration;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.service.dto.TaskDtoService;
import ru.t1.amsmirnov.taskmanager.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
@Category(DBCategory.class)
public class TaskControllerTest {

    @NotNull
    private final TaskWebDto task1 = new TaskWebDto("Test Task 1");

    @NotNull
    private final TaskWebDto task2 = new TaskWebDto("Test Task 2");

    @Autowired
    @NotNull
    private TaskDtoService taskService;

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @Before
    public void init() throws AbstractException {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.add(UserUtil.getUserId(), task1);
        taskService.add(UserUtil.getUserId(), task2);
    }

    @After
    public void clean() throws AbstractException {
        taskService.removeAll();
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void createTest() throws Exception {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.post(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<TaskWebDto> tasks = taskService.findAll(UserUtil.getUserId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(3, tasks.size());
    }

    @Test(expected = ModelNotFoundException.class)
    public void deleteTest() throws Exception {
        @NotNull final String url = "/task/delete/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        taskService.findOneById(UserUtil.getUserId(), task1.getId());
    }

    @Test
    public void editTest() throws Exception {
        @NotNull final String url = "/task/edit/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }


}
