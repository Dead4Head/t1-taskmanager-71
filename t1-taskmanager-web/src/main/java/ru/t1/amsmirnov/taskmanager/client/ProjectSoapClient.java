package ru.t1.amsmirnov.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.IProjectEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ProjectSoapClient {

    public static IProjectEndpoint getInstance(@NotNull final String baseUrl) throws MalformedURLException {
        @NotNull final String wsdl = baseUrl + "/ws/ProjectEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String ns = "http://endpoint.taskmanager.amsmirnov.t1.ru/";
        @NotNull final String service = "ProjectEndpointImplService";
        @NotNull final QName name = new QName(ns, service);
        @NotNull final IProjectEndpoint result = Service.create(url, name).getPort(IProjectEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
