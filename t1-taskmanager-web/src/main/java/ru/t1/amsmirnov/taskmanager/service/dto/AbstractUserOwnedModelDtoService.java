package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.amsmirnov.taskmanager.dto.AbstractUserOwnedModelDto;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.repository.dto.AbstractUserOwnedDtoRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedModelDtoService<M extends AbstractUserOwnedModelDto, R extends AbstractUserOwnedDtoRepository<M>>
        extends AbstractModelDtoService<M, R> {


    public AbstractUserOwnedModelDtoService(@NotNull final R repository) {
        super(repository);
    }

    @NotNull
    @Transactional
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        model.setUserId(userId);
        repository.save(model);
        return model;
    }

    @NotNull
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId, getComparator(comparator));
    }

    @NotNull
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findByUserIdAndId(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Transactional
    public M update(@Nullable final String userId, @Nullable final M model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return update(model);
    }

    @NotNull
    @Transactional
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @Nullable final Optional<M> foundModel = repository.findById(model.getId());
        if (!foundModel.isPresent()) throw new ModelNotFoundException();
        repository.delete(foundModel.get());
        return model;
    }

    @NotNull
    @Transactional
    public M removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> model = repository.findById(id);
        if (!model.isPresent()) throw new ModelNotFoundException();
        repository.delete(model.get());
        return model.get();
    }

    @Transactional
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    public boolean existById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    public long getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

}
