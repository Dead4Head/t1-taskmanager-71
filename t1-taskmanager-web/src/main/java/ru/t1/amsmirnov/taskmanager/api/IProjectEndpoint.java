package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping(value = "/api/projects", produces = "application/json")
public interface IProjectEndpoint {

    @Nullable
    @WebMethod
    ProjectWebDto create(
            @WebParam(name = "name", partName = "name")String name,
            @WebParam(name = "description", partName = "description")String desc
    );

    @Nullable
    @WebMethod
    List<ProjectWebDto> findAll();

    @Nullable
    @WebMethod
    ProjectWebDto findById(String id);

    @Nullable
    @WebMethod()
    ProjectWebDto save(@WebParam(name = "project", partName = "project") ProjectWebDto project);

    @Nullable
    @WebMethod
    ProjectWebDto delete(@WebParam(name = "project", partName = "project") ProjectWebDto project);

    @Nullable
    @WebMethod(operationName = "deleteById")
    ProjectWebDto delete(@WebParam(name = "id", partName = "id") String id);

    @WebMethod
    void deleteAll();

}
