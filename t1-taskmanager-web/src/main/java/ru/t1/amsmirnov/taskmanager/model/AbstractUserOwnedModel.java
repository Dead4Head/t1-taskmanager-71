package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.Nullable;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    protected UserWeb user;

    public AbstractUserOwnedModel() {

    }

    @Nullable
    public UserWeb getUser() {
        return user;
    }

    public void setUser(@Nullable final UserWeb user) {
        this.user = user;
    }

}
