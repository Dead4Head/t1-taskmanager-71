package ru.t1.amsmirnov.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.amsmirnov.taskmanager.dto.ProjectWebDto;
import ru.t1.amsmirnov.taskmanager.dto.Result;
import ru.t1.amsmirnov.taskmanager.dto.UserWebDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("api/auth")
public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    @PostMapping("/login")
    Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    );

    @NotNull
    @WebMethod
    @GetMapping("/profile")
    UserWebDto profile();

    @NotNull
    @WebMethod
    @PostMapping("/logout")
    Result logout();

}
