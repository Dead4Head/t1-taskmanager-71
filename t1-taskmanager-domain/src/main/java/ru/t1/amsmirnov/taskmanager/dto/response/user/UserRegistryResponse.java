package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.UserDTO;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse() {
    }

    public UserRegistryResponse(@Nullable final UserDTO user) {
        super(user);
    }

    public UserRegistryResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}