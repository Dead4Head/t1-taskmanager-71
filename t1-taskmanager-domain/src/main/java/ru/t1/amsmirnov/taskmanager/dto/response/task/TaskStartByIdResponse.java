package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;

public final class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse() {
    }

    public TaskStartByIdResponse(@Nullable final TaskDTO task) {
        super(task);
    }

    public TaskStartByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
